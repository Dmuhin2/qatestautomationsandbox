package com.youtube.test.ui.utils;

import com.youtube.test.commons.Triggers;

/**
 * All browsers that we can use in our webDriver initialization
 *
 * @see BrowserPool
 */
public enum BrowserType {

    IE("internet explorer", "Windows 10", Triggers.IE_VERSION),
    CH("chrome", "Windows 10", Triggers.CH_VERSION),
    FF("firefox", "Windows 10", Triggers.FF_VERSION),
    EDGE("MicrosoftEdge", "Windows 10", Triggers.EDGE_VERSION),
    SAFARI("safari", "macOS 10.14", Triggers.SAFARI_VERSION);

    private String browserName;
    private String platform;
    private String version;

    BrowserType(String browserName, String platform, String version) {
        this.browserName = browserName;
        this.platform = platform;
        this.version = version;
    }

    /**
     * @return name for SauseLab remote execution
     */
    public String getBrowserName() {
        return browserName;
    }

    /**
     * @return platform of executable browser (ex: Windows 10/ macOs 10.14 / ubuntu 18.04)
     */
    public String getPlatform() {
        return platform;
    }

    /**
     * @return version of executable browser
     * @see Triggers
     */
    public String getVersion() {
        return version;
    }
}
