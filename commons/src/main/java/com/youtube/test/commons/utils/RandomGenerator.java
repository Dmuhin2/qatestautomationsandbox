package com.youtube.test.commons.utils;

import java.util.Random;

/**
 * Генератор рандома.
 */
public class RandomGenerator {

    public static String generateNumber(int length) {
        Random generator = new Random();
        String number ="";
        while (number.length()!=length){
            number= number+generator.nextInt(10);
        }
        return number;
    }
}
