package com.youtube.test.test_suite;

import com.youtube.test.commons.utils.LoggerUtil;
import io.qameta.allure.Allure;
import org.testng.annotations.AfterClass;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Base class for extending from all other our "base" classes
 * The class describes all the methods that must be run in each test.
 */
public abstract class BaseTest {

    @AfterClass
    public void tearDownBaseTestClass() {
        saveLogs();
    }

    /**
     * Attaching test log to allure
     */
    private void saveLogs() {
        Path content = Paths.get(LoggerUtil.getLoggerName());
        try (InputStream is = Files.newInputStream(content)) {
            Allure.addAttachment("Log from test", is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
