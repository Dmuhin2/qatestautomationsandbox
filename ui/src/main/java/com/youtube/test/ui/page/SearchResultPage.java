package com.youtube.test.ui.page;

import com.youtube.test.ui.utils.Browser;
import org.openqa.selenium.By;

/**
 * YouTube's page with filtered search results
 */
public class SearchResultPage extends BasePage {

    SearchResultPage(Browser browser) {
        super(browser);
    }

    /**
     * Opening video on search page by click on picture of this video
     *
     * @param numberOfVideo position of openable video
     * @return opened video page object
     */
    public VideoPage openVideoFromResult(int numberOfVideo) {
        actions.clickOnElement(By.xpath("//div[@id='contents']/ytd-video-renderer[" + numberOfVideo + "]/div[@id='dismissable']/ytd-thumbnail/a[@id='thumbnail']"));
        return new VideoPage(browser);
    }
}
