package com.youtube.test.ui.utils;

import com.youtube.test.commons.utils.LoggerUtil;

/**
 * The class contains templates for implementing in Action class
 *
 * @see Actions
 */
class ExceptionCatcher {

    /**
     * Template for action.
     * Logging of the executable method of the pageObject class
     * Implementing business logic using lambda
     * Catch an error, register and throw away
     *
     * @param action implementing business logic for execution
     */
    protected static void exceptionCatcherVoid(InterfaceForVoidActions action) {
        try {
            LoggerUtil.currentMethodData();
            action.voidAction();
        } catch (Exception e) {
            LoggerUtil.error("Can't complete because:\n", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Template for action that return String value.
     * Logging of the executable method of the pageObject class
     * Implementing business logic using lambda
     * Catch an error, register and throw away
     *
     * @param action implementing business logic for execution
     */
    protected static String exceptionCatcherString(InterfaceForStringActions action) {
        try {
            LoggerUtil.currentMethodData();
            return action.stringAction();
        } catch (Exception e) {
            LoggerUtil.error("Can't complete because:\n", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Function for implementation some java logic with void return
     */
    protected interface InterfaceForVoidActions {
        void voidAction();
    }

    /**
     * Function for implementation some java logic with String return
     */
    protected interface InterfaceForStringActions {
        String stringAction();
    }
}
