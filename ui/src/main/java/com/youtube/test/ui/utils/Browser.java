package com.youtube.test.ui.utils;

import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

/**
 * The class for setting base Selenium WebDriver commands
 * Ex: setting timeouts, managing size , closing , etc.
 */
public class Browser {

    private final WebDriver driver;

    /**
     * Setting web driver for working with it
     *
     * @param driver selenium web driver
     */
    Browser(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Setting the “implicit wait” to wait for all uncached “explicit expectations”
     *
     * @param timeoutTime how many seconds to wait
     * @return Browser class object
     */
    public Browser setImplicitlyWaitTimeout(long timeoutTime) {
        driver.manage().timeouts().implicitlyWait(timeoutTime, TimeUnit.SECONDS);
        return this;
    }

    /**
     * Opening browser as maximal size
     *
     * @return Browser class object
     */
    public Browser maximizeBrowser() {
        driver.manage().window().maximize();
        return this;
    }

    /**
     * Setting “page load wait” to wait for the browser page load
     *
     * @param timeoutTime how many seconds to wait
     * @return Browser class object
     */
    public Browser setPageLoadWaitTimeout(long timeoutTime) {
        driver.manage().timeouts().pageLoadTimeout(timeoutTime, TimeUnit.SECONDS);
        return this;
    }

    /**
     * @return currently executing webDriver
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Close browser that been created from BrowserPool
     */
    public void cleanBrowser() {
        driver.quit();
    }
}
