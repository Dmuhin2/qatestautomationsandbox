package com.youtube.test.test_suite.ui;

import com.youtube.test.commons.Triggers;
import com.youtube.test.test_suite.BaseTest;
import com.youtube.test.ui.page.Menu;
import com.youtube.test.ui.utils.Browser;
import com.youtube.test.ui.utils.BrowserPool;
import com.youtube.test.ui.utils.BrowserType;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Base class for extending from all other our "UI test" classes
 * The class describes all the methods that must be run in each UI test.
 */
public abstract class BaseUITest extends BaseTest {

    Menu menu;
    private Browser browser;

    /**
     * Setting the time of the .log file and initializing the browser with settings
     */
    @BeforeClass(groups = "uiTests")
    @Parameters({"browserName"})
    public void setUpBaseUITestClass(String browserName) {
        System.setProperty("date.time", new SimpleDateFormat(Triggers.DATA_TIME_LOGGER_FORMAT).format(new Date()));
        browser = BrowserPool.getBrowser(BrowserType.valueOf(browserName))
                .setImplicitlyWaitTimeout(Triggers.TIMEOUT_TIME)
                .setPageLoadWaitTimeout(Triggers.TIMEOUT_TIME)
                .maximizeBrowser();
        menu = new Menu(browser);
    }

    /**
     * If the browser is not null, then taking a screenshot and closing it.
     * If the browser is null, then just closing it.
     */
    @AfterClass(groups = "uiTests")
    public void tearDownBaseUITestClass() {
        if (browser.getDriver() != null) {
            saveScreen();
        }
        browser.cleanBrowser();
    }

    /**
     * Attaching screen to Allure
     */
    @Attachment(value = "Screenshot", type = "image/png")
    private byte[] saveScreen() {
        return ((TakesScreenshot) browser.getDriver()).getScreenshotAs(OutputType.BYTES);
    }


}
