package com.youtube.test.ui.utils;

import com.youtube.test.commons.Constants;
import com.youtube.test.commons.Triggers;
import com.youtube.test.commons.utils.LoggerUtil;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.EdgeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * The class for creating webDriver object as local or remote using sauceLabs
 */
public class BrowserPool {

    private BrowserPool() {
        throw new IllegalArgumentException("You can`t create object of this class. Please use \"getBrowser\" method to initialize browser.");
    }

    /**
     * Initializing and creating webDriver
     *
     * @param browserType what browser will we use for the local session
     * @return browser class object for listening
     * @see BrowserType
     */
    public static Browser getBrowser(BrowserType browserType) {
        WebDriver driver;
        if (Triggers.IS_REMOTE) {
            driver = createRemoteWebDriver(browserType);
        } else {
            driver = createLocalWebDriver(browserType);
        }
        return new Browser(driver);
    }

    /**
     * Creating remote sauceLab web driver object based on browser type
     *
     * @param browserType what browser will we use for the local session
     * @return ready for usage remote web driver
     * @see BrowserType
     */
    private static WebDriver createRemoteWebDriver(BrowserType browserType) {
        WebDriver driver = null;
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("username", Constants.SAUSELABS_USERNAME);
        capabilities.setCapability("accessKey", Constants.SAUSELABS_ACCESS_KEY);
        capabilities.setCapability("browserName", browserType.getBrowserName());
        capabilities.setCapability("platform", browserType.getPlatform());
        capabilities.setCapability("version", browserType.getVersion());
        capabilities.setCapability("build", browserType.name());
        capabilities.setCapability("name", browserType.name() + " Sauce Lab Tests");
        try {
            driver = new RemoteWebDriver(new URL(Constants.SAUSELABS_URL), capabilities);
        } catch (MalformedURLException e) {
            LoggerUtil.error("Something wrong in remote web driver init:", e);
        }
        return driver;
    }

    /**
     * Creating local web driver object based on browser type
     *
     * @param browserType what browser will we use for the local session
     * @return ready for usage local web driver
     * @see BrowserType
     */
    private static WebDriver createLocalWebDriver(BrowserType browserType) {
        switch (browserType) {
            case IE:
                InternetExplorerDriverManager.getInstance().version(Triggers.IE_VERSION).setup();
                return new InternetExplorerDriver();
            case CH:
                ChromeDriverManager.getInstance().version(Triggers.CH_VERSION).setup();
                return new ChromeDriver();
            case FF:
                FirefoxDriverManager.getInstance().version(Triggers.FF_VERSION).setup();
                return new FirefoxDriver();
            case EDGE:
                EdgeDriverManager.getInstance().version(Triggers.EDGE_VERSION).setup();
                return new EdgeDriver();
            default:
                throw new IllegalArgumentException(browserType.name() + " isn't yet implemented");
        }
    }


}
