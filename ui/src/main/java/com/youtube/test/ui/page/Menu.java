package com.youtube.test.ui.page;

import com.youtube.test.commons.utils.LoggerUtil;
import com.youtube.test.ui.utils.Browser;

/**
 * Page object class for navigation to pages
 */
public class Menu extends BasePage {

    public Menu(Browser browser) {
        super(browser);
    }

    /**
     * Opening youtube page and asserting the title
     *
     * @return Youtube home page
     */
    public HomePage goToHomePage() {
        browser.getDriver().get("https://www.youtube.com/");
        if (!"YouTube".equals(driver.getTitle())) {
            LoggerUtil.error("This is not the youtube page");
            throw new IllegalStateException("This is not the youtube page");
        }
        return new HomePage(browser);
    }
}
