package com.youtube.test.ui.page;

import com.youtube.test.ui.utils.Browser;
import org.openqa.selenium.By;

/**
 * Youtube's channel page
 */
public class ChannelPage extends BasePage {
    ChannelPage(Browser browser) {
        super(browser);
    }

    /**
     * Click on "Subscribe" button
     *
     * @return this
     */
    public ChannelPage clickSubscribeButton() {
        actions.clickOnElement(By.xpath("//div[@id='channel-header-container']/div[@id='inner-header-container']/div[@id='buttons']/div[@id='subscribe-button']"));
        return this;


    }

    /**
     * Get text from pop-up "subscribe" button opened by click on "subscribe" button of current channel.
     *
     * @return text from "subscribe" button.
     */
    public String getEnterButtonTextFromSubscribePopUp() {
        return actions.getTextFromElement(By.xpath("//paper-button[@class='style-scope ytd-button-renderer style-blue-text size-default' and @id='button']"));
    }
}
