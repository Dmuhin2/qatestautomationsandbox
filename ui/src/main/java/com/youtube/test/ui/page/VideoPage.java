package com.youtube.test.ui.page;

import com.youtube.test.ui.utils.Browser;
import org.openqa.selenium.By;

/**
 * Youtube's video page
 */
public class VideoPage extends BasePage {
    VideoPage(Browser browser) {
        super(browser);
    }

    /**
     * Click on the video author's avatar
     *
     * @return author's channel page object
     */
    public ChannelPage clickOnAuthorAvatar() {
        actions.clickOnElement(By.xpath("//div[@id='meta-contents']/ytd-video-secondary-info-renderer/div[@id='container']/div[@id='top-row']/ytd-video-owner-renderer/" +
                "a[@class='yt-simple-endpoint style-scope ytd-video-owner-renderer']"));
        return new ChannelPage(browser);
    }
}
