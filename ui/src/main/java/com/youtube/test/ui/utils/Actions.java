package com.youtube.test.ui.utils;

import com.youtube.test.commons.utils.LoggerUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * The class with WebDriver action methods implementation
 * for working with webElements and logging result of execution
 *
 * @see ExceptionCatcher
 */
public class Actions extends ExceptionCatcher {

    private final WebDriverWait wait;
    private final WebDriver driver;

    public Actions(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    /**
     * Entering text into input webElement
     *
     * @param input - веб-элемент поля для ввода
     * @param text  - текст который необходимо прописать
     * @return Actions class object
     */
    public Actions enterTextInToInput(By input, String text) {
        exceptionCatcherVoid(() -> {
            wait.until(ExpectedConditions.elementToBeClickable(input));
            // driver.findElement(input).clear();
            driver.findElement(input).sendKeys(text);
            LoggerUtil.info("Text: \"" + text + "\" was inputed into input: \"" + input + "\"");
        });
        return this;
    }

    /**
     * Waiting until element to be clickable and clicking on it
     *
     * @param by Selenium locator
     * @return Actions class object
     * @see By
     */
    public Actions clickOnElement(By by) {
        exceptionCatcherVoid(() -> {
            wait.until(ExpectedConditions.elementToBeClickable(by));
            LoggerUtil.info("Element \"" + by + "\" is clickable");
            driver.findElement(by).click();
            LoggerUtil.info("Element:\"" + by + "\" clicked");
        });
        return this;
    }

    /**
     * Getting and logging text that been taken from webElement
     *
     * @param by Selenium locator
     * @return received text from webElement
     * @see By
     */
    public String getTextFromElement(By by) {
        return exceptionCatcherString(() -> {
            String textFromElement = driver.findElement(by).getText();
            LoggerUtil.info("Text from WebElement \"" + by + "\" is: \"" + textFromElement + "\"");
            return textFromElement;
        });
    }

    /**
     * Execute javaScript by Selenium
     *
     * @param javaScript javaScript for execution
     * @return Actions class object
     */
    public Actions executeJavaScript(String javaScript) {
        ((JavascriptExecutor) driver).executeScript(javaScript);
        return this;
    }
}
