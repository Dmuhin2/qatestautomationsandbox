package com.youtube.test.commons;

public class Triggers {
    //Runtime triggers
    public static final boolean IS_REMOTE = true;
    public static final String BROWSER_NAME = "CH";
    //Browsers version
    public static final String IE_VERSION = "11.285";
    public static final String CH_VERSION = "74.0";
    public static final String FF_VERSION = "66.0";
    public static final String EDGE_VERSION = "16.16299";
    public static final String SAFARI_VERSION = "12.0";
    //WebDriver params
    public static final int TIMEOUT_TIME = 10;
    //Logger params
    public static final String DATA_TIME_LOGGER_FORMAT = "dd.MM.yyyy HH-mm-ss";
}
