package com.youtube.test.ui.page;

import com.youtube.test.commons.utils.RandomGenerator;
import com.youtube.test.ui.utils.Browser;
import org.openqa.selenium.By;

/**
 * Youtube's home page
 */
public class HomePage extends BasePage {

    HomePage(Browser browser) {
        super(browser);
    }

    /**
     * Нажать на результат поиска в выпадающем меню вариантов по номеру строки (отчёт происходит сверху вниз)
     *
     * @param numberOfSearchResult номер строки результата поиска
     * @return страница результатов поиска.
     */
    public SearchResultPage clickOnSearchResult(int numberOfSearchResult) {
        actions.clickOnElement(By.id("sbse" + (numberOfSearchResult - 1)));
        return new SearchResultPage(browser);
    }

    /**
     * Ввести текст в строку поиска.
     * !!!Разделён на 2 ввода.
     * !!!Если всё вводить сплошным текстом, браузер не успевает прогрузить выпадающее меню с предложенными результатами.
     *
     * @return this
     */
    public HomePage enterSearchFilter() {
        String randomNumber = RandomGenerator.generateNumber(4);
        actions.enterTextInToInput(By.xpath("//input[@id='search']"), randomNumber.substring(0, 3));
        actions.enterTextInToInput(By.xpath("//input[@id='search']"), randomNumber.substring(3));
        return this;
    }
}
