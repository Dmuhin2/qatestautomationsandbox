package com.youtube.test.test_suite.ui;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;


public class YoutubeTest extends BaseUITest {

    @Feature("Youtube")
    @Story("User channel")
    @Description("The test verifies that is unauthorized user cannot subscribe on Youtube channel")
    @Test(groups = "uiTests", description = "Attempting to subscribe to the Youtube channel by an unauthorized user.")
    public void testUnauthorizedSubscribeAttempting() {
        String buttonText = menu.goToHomePage()
                .enterSearchFilter()
                .clickOnSearchResult(2)
                .openVideoFromResult(4)
                .clickOnAuthorAvatar()
                .clickSubscribeButton()
                .getEnterButtonTextFromSubscribePopUp();
        Assert.assertEquals(buttonText, "SIGN IN");
    }


}