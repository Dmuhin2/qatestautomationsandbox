package com.youtube.test.commons.utils;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

/**
 * Logger util
 * Created for working only with one logger object as statics methods
 */
public class LoggerUtil {

    private static final Logger logger = Logger.getLogger(LoggerUtil.class);

    public static void info(String info) {
        logger.info(info);
    }

    /**
     * Logging and throwing error msg
     *
     * @param error msg to log
     * @param e     error to throw
     */
    public static void error(String error, Throwable e) {
        logger.error(error, e);
        throw new RuntimeException(e);
    }

    /**
     * Logging error msg
     *
     * @param error msg to log
     */
    public static void error(String error) {
        logger.error(error);
    }

    /**
     * Logging debug msg
     *
     * @param debug msg to log
     */
    public static void debug(String debug) {
        logger.debug(debug);
    }

    /**
     * Logging warm msg
     *
     * @param warn msg to log
     */
    public static void warn(String warn) {
        logger.warn(warn);
    }

    /**
     * @return absolute path to logger
     */
    public static String getLoggerName() {
        String logPath = "";
        Enumeration e = Logger.getRootLogger().getAllAppenders();
        while (e.hasMoreElements()) {
            Appender app = (Appender) e.nextElement();
            if (app instanceof FileAppender) {
                logPath = ((FileAppender) app).getFile();
            }
        }
        return logPath;
    }

    /**
     * Log info about current executed method
     */
    public static void currentMethodData() {
        info(stackTraceParser("page.", 1));
    }

    /**
     * Log current executed method/class/frame line and test line
     *
     * @param contains          keywords for filtering and founding in stackTrace
     * @param numberOfTestTrace number of line in stackTrace for searching after filtering
     * @return log message
     */
    private static String stackTraceParser(String contains, int numberOfTestTrace) {
        String returner = "";
        List<StackTraceElement> list = Arrays.asList(Thread.currentThread().getStackTrace());
        for (StackTraceElement element : list) {
            if (element.getClassName().contains(contains)) {
                String className = element.getClassName();
                String methodName = element.getMethodName();
                int line = element.getLineNumber();
                int TestLine = list.get(list.indexOf(element) + numberOfTestTrace).getLineNumber();
                returner = "[Class:\"" + className + "\"_Method:\"" + methodName + "\"_Line:" + line + "_TestLine:" + TestLine + "]";
                break;
            }
        }
        return returner;
    }
}
