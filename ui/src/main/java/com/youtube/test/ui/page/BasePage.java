package com.youtube.test.ui.page;

import com.youtube.test.ui.utils.Actions;
import com.youtube.test.ui.utils.Browser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Basic initialization page for extending from any class of page object
 */
abstract class BasePage {
    Browser browser;
    WebDriver driver;
    Actions actions;

    /**
     * Initialization of main selenium objects
     *
     * @param browser initialized browser from Browser pool
     * @see com.youtube.test.ui.utils.BrowserPool
     */
    BasePage(Browser browser) {
        this.browser = browser;
        this.driver = browser.getDriver();
        this.actions = new Actions(driver, new WebDriverWait(driver, 15));
    }
}
